import email_listener

# https://thejpanda.com/2021/01/07/automation-monitoring-gmail-inbox-using-python/
# https://pypi.org/project/email-listener/

# email: kim.heirman@hkm-services.be
# password:
# folder: inbox
email = "kim.heirman@hkm-services.be"
app_password = "##"
folder = "Inbox"


el = email_listener.EmailListener(email, app_password, folder)

el.login()

